#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

#include <QMessageBox>

class Exceptions
{
public:
    Exceptions(int error_code)
    {
         switch(error_code)
         {
         case 1: QMessageBox::information(NULL , "Ошибка!", "Ошибка открытия файла!"); break;
         case 2: QMessageBox::information(NULL , "Ошибка", "Ошибка выделения памяти!"); break;
         case 3: QMessageBox::information(NULL , "Ошибка", "Невозможно решить!"); break;
         default: break;
         }
    }
};

#endif // EXCEPTIONS_H
