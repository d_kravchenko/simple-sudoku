#include "sudoku.h"
#include "ui_sudoku.h"

Sudoku::Sudoku(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Sudoku)
{
    ui->setupUi(this);
    this->setWindowIcon(QIcon(":/icons/app-sudoku.png"));
    k_= 0;
    pos_= 0;
    pos_solve_= 0;
    try
    {
        solver = new Solver();
        if (!solver) throw Exceptions(2);
        ui->tableWidget->setMinimumSize(sizeHint());
        for (int i=0; i<9; i++)
            for (int j=0; j<9; j++)
            {
                newItem = new QTableWidgetItem;
                if (!newItem) throw Exceptions(2);
                newItem->setTextAlignment(Qt::AlignCenter);
                newItem->setBackgroundColor(QColor(255,255,224));
                newItem->setTextColor(QColor(0,0,255));
                ui->tableWidget->setItem(i,j,newItem);
            }
    }
    catch(int){}
}

void Sudoku::createGame()
{
    k_++;
    clean();
    load();
    setSudoku();
}

void Sudoku::checkCell(int row, int column)
{
    if (isReady_)
    {
        int i = ((ui->tableWidget->item(row,column)->text()).toInt());
        if (i < 1 || i > 9)
            ui->tableWidget->item(row,column)->setText((QString)"");
         else if (!(checkRow(row,column,i) && checkColumn(row,column,i) && checkGrid(row,column,i)))
               {
                    isReady_=false;
                    ui->tableWidget->item(row,column)->setText((QString)"");
                    isReady_=true;
               }
         else
            sudoku_[row][column]=i;
    }
}

bool Sudoku::checkRow(int row,int column,int num)
{
    int c;
    for (c=0; c<9; c++)
    {
        if(((ui->tableWidget->item(row,c)->text()).toInt())==num && c!=column)
        {
            ui->tableWidget->setCurrentCell(row,c,QItemSelectionModel::SelectCurrent);
            return false;
        }
    }
    return true;
}

bool Sudoku::checkColumn(int row,int column,int num)
{
    int r;
    for (r=0; r<9; r++)
        if (((ui->tableWidget->item(r,column)->text()).toInt())==num && r!=row)
        {
            ui->tableWidget->setCurrentCell(r,column,QItemSelectionModel::SelectCurrent);
            return false;
        }
    return true;
}

bool Sudoku::checkGrid(int row,int column,int num)
{
    if (row<3 && column<3)
    {
        for (int i=0; i<3; i++)
            for (int j=0; j<3; j++)
                if (((ui->tableWidget->item(i,j)->text()).toInt())==num && row!=i && column!=j)
                {
                    ui->tableWidget->setCurrentCell(i,j,QItemSelectionModel::SelectCurrent);
                    return false;
                }

    }
        else if (row<3 && column>=3 && column<6)
        {
            for (int i=0; i<3; i++)
                for (int j=3; j<6; j++)
                    if (((ui->tableWidget->item(i,j)->text()).toInt())==num && row!=i && column!=j)
                    {
                        ui->tableWidget->setCurrentCell(i,j,QItemSelectionModel::SelectCurrent);
                        return false;
                    }
        }
        else if (row<3 && column>=6)
        {
            for (int i=0; i<3; i++)
                for (int j=6; j<9; j++)
                    if (((ui->tableWidget->item(i,j)->text()).toInt())==num && row!=i && column!=j)
                    {
                        ui->tableWidget->setCurrentCell(i,j,QItemSelectionModel::SelectCurrent);
                        return false;
                    }
        }
        else if (row>=3 && row<6 && column<3)
        {
            for (int i=3; i<6; i++)
                for (int j=0; j<3; j++)
                    if (((ui->tableWidget->item(i,j)->text()).toInt())==num && row!=i && column!=j)
                    {
                        ui->tableWidget->setCurrentCell(i,j,QItemSelectionModel::SelectCurrent);
                        return false;
                    }
        }
        else if (row>=3 && row<6 && column>=3 && column<6)
        {
            for (int i=3; i<6; i++)
                for (int j=3; j<6; j++)
                    if (((ui->tableWidget->item(i,j)->text()).toInt())==num && row!=i && column!=j)
                    {
                        ui->tableWidget->setCurrentCell(i,j,QItemSelectionModel::SelectCurrent);
                        return false;
                    }
        }
        else if (row>=3 && row<6 && column>=6)
        {
            for (int i=3; i<6; i++)
                for (int j=6; j<9; j++)
                    if (((ui->tableWidget->item(i,j)->text()).toInt())==num && row!=i && column!=j)
                    {
                        ui->tableWidget->setCurrentCell(i,j,QItemSelectionModel::SelectCurrent);
                        return false;
                    }
        }
        else if (row>=6 && column<3)
        {
            for (int i=6; i<9; i++)
                for (int j=0; j<3; j++)
                    if (((ui->tableWidget->item(i,j)->text()).toInt())==num && row!=i && column!=j)
                    {
                        ui->tableWidget->setCurrentCell(i,j,QItemSelectionModel::SelectCurrent);
                        return false;
                    }
        }
        else if (row>=6 && column>=3 && column<6)
        {
            for (int i=6; i<9; i++)
                for (int j=3; j<6; j++)
                    if (((ui->tableWidget->item(i,j)->text()).toInt())==num && row!=i && column!=j)
                    {
                        ui->tableWidget->setCurrentCell(i,j,QItemSelectionModel::SelectCurrent);
                        return false;
                    }
        }
        else if (row>=6 && column>=6)
        {
            for (int i=6; i<9; i++)
                for (int j=6; j<9; j++)
                    if (((ui->tableWidget->item(i,j)->text()).toInt())==num && row!=i && column!=j)
                    {
                        ui->tableWidget->setCurrentCell(i,j,QItemSelectionModel::SelectCurrent);
                        return false;
                    }
        }
     return true;
}

void Sudoku::clean()
{
    for (int i=0; i<9; i++)
        for (int j=0; j<9; j++)
        {
            ui->tableWidget->item(i,j)->setText("");
            ui->tableWidget->item(i,j)->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled|Qt::ItemIsEditable);
            sudoku_[i][j]=0;
            solved_[i][j]=0;
        }
}

void Sudoku::load()
{
    QFile file(":/icons/sudoku.txt");
    try
    {
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        throw Exceptions(1);
    }
    catch(int) {}
    if (pos_!= 0)
        file.seek(pos_);
    for (int i=0; i<9; i++)
    {
        if (file.atEnd())
        {
            pos_= 0;
            file.seek(pos_);
        }
        QString array = file.readLine();
        QStringList arrayList = array.split(' ');
        for(int j=0; j<arrayList.size(); j++)
        {
            sudoku_[i][j] = (arrayList[j]).toInt();
            solved_[i][j] = sudoku_[i][j];
        }
    }
    QString array = file.readLine();
    pos_=(file.pos());
    file.close();

}

void Sudoku::solveGame()
{
    isReady_ = false;
    if( k_>=1)
    {
        if (solver->checkData(solved_) && solver->checkGrid(solved_))
        {
            try
            {
                if (!solver->solve(solved_))
                    throw(3);
            }
            catch(int) { Exceptions(3); return;}
            for (int i=0; i<9; i++)
                for (int j=0; j<9; j++)
                    ui->tableWidget->item(i,j)->setText(QString::number(solved_[i][j]));

        }
    }
    isReady_ = true;
}

void Sudoku::openGame()
{
    k_++;
    clean();
    QFile loadFile;
    QString fileName;

    fileName = QFileDialog::getOpenFileName(this, QString("Открыть"),"",tr("TXT (*.txt)"));
    loadFile.setFileName(fileName);
    if (!fileName.isEmpty())
    {
        try
        {
            if (!loadFile.open(QIODevice::ReadOnly | QIODevice::Text))
            throw Exceptions(1);
        }
        catch(int){}
        for (int i=0; i<9; i++)
        {
            if (!loadFile.atEnd())
            {
                QString array = loadFile.readLine();
                QStringList arrayList = array.split(' ');
                for (int j=0; j<arrayList.size(); j++)
                {
                    sudoku_[i][j] = (arrayList[j]).toInt();
                    solved_[i][j] = sudoku_[i][j];
                }

            }
        }
        setSudoku();
        loadFile.close();
    }

}

void Sudoku::setSudoku()
{
    isReady_=false;
    for (int i=0; i<9; i++)
        for (int j=0; j<9; j++)
        {
            if (sudoku_[i][j]==0)
                ui->tableWidget->item(i,j)->setText((QString)"");
             else
             {
                 ui->tableWidget->item(i,j)->setText(QString::number(sudoku_[i][j]));
                 ui->tableWidget->item(i,j)->setFlags(Qt::ItemIsSelectable);
             }
        }
    isReady_=true;
}

Sudoku::~Sudoku()
{
    delete ui;
}
