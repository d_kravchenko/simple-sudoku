#ifndef SUDOKU_H
#define SUDOKU_H

#include <QMainWindow>
#include <QTableWidgetItem>
#include <QFile>
#include <QFileDialog>
#include <QStringList>
#include "solver.h"
#include "exceptions.h"

namespace Ui {
class Sudoku;
}

class Sudoku : public QMainWindow
{
    Q_OBJECT

protected:
    Ui::Sudoku* ui;
    QTableWidgetItem* newItem;
    Solver* solver;
    int k_;
    int sudoku_[9][9];
    int solved_[9][9];
    bool isReady_;
    qint64 pos_,pos_solve_;
public:
    explicit Sudoku(QWidget *parent = 0);
    bool checkRow (int row, int column, int num);
    bool checkColumn (int row, int column, int num);
    bool checkGrid (int row,int column,int num);
    void setSudoku();
    void load();
    void clean();
    ~Sudoku();
public slots:
    void checkCell (int row, int column);
    void openGame();
    void createGame();
    void solveGame();

};

#endif // SUDOKU_H
