#ifndef SOLVER_H
#define SOLVER_H

#include <cmath>
#include <vector>
#include <algorithm>

using namespace std;

struct Coord
{
    int x;
    int y;
};

enum{SEARCH, CANDIDATE, STOP};

class Solver
{
    int grid[9][9];
public:
    bool solve(int grid[][9]);
    bool isInRow(int rowNumber, int number, int grid[][9]);
    bool isInColumn(int columnNumber, int number, int grid[][9]);
    bool isInZone(Coord zone, int number, int grid[][9]);
    bool solveZones(int grid[][9]);
    bool solveRows(int grid[][9]);
    bool solveColumns(int grid[][9]);
    bool solveSquares(int grid[][9]);
    bool isSolved(int grid[][9]);
    bool checkGrid(int grid[][9]);
    bool checkData(int grid[][9]);
};

#endif // SOLVER_H
