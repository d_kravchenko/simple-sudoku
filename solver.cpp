#include "solver.h"

bool Solver::solve(int grid[][9])
{
    bool c=true, a, b, d, e;
    while(c)
    {
        a=solveZones(grid);
        b=solveColumns(grid);
        d=solveRows(grid);
        e=solveSquares(grid);
        if(!a && !b && !d && !e)
        {
            c=false;
        }
    }
    if(isSolved(grid))
    {
        return true;
    }
    else
    {
        return false;
    }

}

bool Solver::solveZones(int grid[][9])
{
    int a = SEARCH;
    bool c = false;
    Coord square;
    square.x = 0;
    square.y = 0;
    Coord zone;
    for(int i=0; i<=2; i++)
    {
        for (int j=0; j<=2; j++) // Для каждой зоны 3*3
        {
            for (int k=1; k<=9; k++) // Для каждого числа
            {
                zone.x=i;
                zone.y=j;
                if (!isInZone(zone, k, grid))
                {
                    for (int column=(3*i); column<=(3*i+2); column++)
                    {
                        for (int row=(3*j); row<=(3*j+2); row++) // Для каждой сетки 3*3
                        {
                            if (grid[column][row]==0)
                            {
                                if (!isInRow(row, k, grid) && !isInColumn(column, k, grid))
                                {
                                    if (a==SEARCH)
                                    {
                                        a=CANDIDATE;
                                        square.x=column;
                                        square.y=row;
                                    }
                                    else if (a==CANDIDATE)
                                    {
                                        a=STOP;
                                    }
                                }
                            }
                        }
                    }
                    if (a==CANDIDATE)
                    {
                        grid[square.x][square.y]=k;
                        c=true;
                    }
                    a=SEARCH;
                }
            }
        }
    }
    return c;
}

bool Solver::solveRows(int grid[][9])
{
    int a=SEARCH;
    bool c=false;
    Coord square;
    square.x=0;
    square.y=0;
    Coord zone;
    for(int i=0; i<=8; i++) // Для каждой строки
    {
        for(int j=1; j<=9; j++) //Для каждого числа
        {
            if(!isInRow(i, j, grid))
            {
                for(int k=0; k<=8; k++) // Для каждой клетки
                {
                    if(grid[k][i]==0)
                    {
                        zone.x=floor((float)k/3);
                        zone.y=floor((float)i/3);
                        if(!isInZone(zone, j, grid) && !isInColumn(k, j, grid))
                        {
                            if(a==SEARCH)
                            {
                                a=CANDIDATE;
                                square.x=k;
                                square.y=i;
                            }
                            else if(a==CANDIDATE)
                            {
                                a=STOP;
                            }
                        }
                    }
                }
                if(a==CANDIDATE)
                {
                    grid[square.x][square.y]=j;
                    c=true;
                }
                a=SEARCH;
            }
        }
    }
    return c;
}

bool Solver::solveColumns(int grid[][9])
{
    int a=SEARCH;
    bool c=false;
    Coord square;
    square.x=0;
    square.y=0;
    Coord zone;
    for(int i=0; i<=8; i++) // Для каждой колонки
    {
        for(int j=1; j<=9; j++) // Для каждого числа
        {
            if(!isInColumn(i, j, grid))
            {
                for(int k=0; k<=8; k++) // Для каждой клетки
                {
                    if(grid[i][k]==0)
                    {
                        zone.x=floor((float)i/3);
                        zone.y=floor((float)k/3);
                        if(!isInZone(zone, j, grid) && !isInRow(k, j, grid))
                        {
                            if(a==SEARCH)
                            {
                                a=CANDIDATE;
                                square.x=i;
                                square.y=k;
                            }
                            else if(a==CANDIDATE)
                            {
                                a=STOP;
                            }
                        }
                    }
                }
                if(a==CANDIDATE)
                {
                    grid[square.x][square.y]=j;
                    c=true;
                }
                a=SEARCH;
            }
        }
    }
    return c;
}

bool Solver::solveSquares(int grid[][9])
{
    int a=SEARCH, b=0;
    bool c=false;
    Coord zone;
    Coord square;
    for(int i=0; i<=8; i++)
    {
        for(int j=0; j<=8; j++) // Для каждой клетки
        {
            if(grid[i][j]==0)
            {
                zone.x=floor((float)i/3);
                zone.y=floor((float)j/3);
                for(int k=1; k<=9; k++) // Для каждого числа
                {
                    if(!isInRow(j, k, grid) && !isInColumn(i, k, grid) && !isInZone(zone, k, grid))
                    {
                        if(a==SEARCH)
                        {
                            a=CANDIDATE;
                            b=k;
                            square.x=i;
                            square.y=j;
                        }
                        else if(a==CANDIDATE)
                        {
                            a=STOP;
                            b=0;
                        }
                    }
                }
            }
            if(a==CANDIDATE)
            {
                grid[square.x][square.y]=b;
                c=true;
            }
            a=SEARCH;
        }
    }
    return c;
}

bool Solver::checkGrid(int grid[][9])
{
    bool gridCheck = true;
    int number=0;
    Coord zone;
    for(int i=0; i<=8; i++)
    {
        for(int j=0; j<=8; j++)
        {
            if(grid[i][j]!=0)
            {
                zone.x=floor((float)i/3);
                zone.y=floor((float)j/3);
                number=grid[i][j];
                grid[i][j]=0;
                if(isInColumn(i, number, grid) || isInRow(j, number, grid) || isInZone(zone, number, grid))
                {
                    gridCheck=false;
                }
                grid[i][j]=number;
            }
        }
    }
    return gridCheck;
}

bool Solver::isSolved(int grid[][9])
{
    vector<int> numbers;
    for(int i=0; i<=8; i++)
    {
        for(int j=0; j<=8; j++)
        {
            if(grid[i][j]==0 || grid[i][j]<=0 || grid[i][j]>9)
            {
                return false;
            }
            if(binary_search(numbers.begin(), numbers.end(), grid[i][j]))
            {
                return false;
            }
            else
            {
                numbers.push_back(grid[i][j]);
            }
        }
        numbers.clear();
    }
    return true;
}

bool Solver::isInRow(int rowNumber, int number, int grid[][9])
{
    if(rowNumber >= 0 && rowNumber <=8 && number >=1 && number <=9)
    {
        for(int i=0; i<9; i++)
        {
            if(grid[i][rowNumber] == number)
            {
                return true;
            }
        }
    }
    return false;
}

bool Solver::isInColumn(int columnNumber, int number, int grid[][9])
{
    if(columnNumber >= 0 && columnNumber <=8 && number >=1 && number <=9)
    {
        for(int i=0; i<9; i++)
        {
            if(grid[columnNumber][i] == number)
            {
                return true;
            }
        }
    }
    return false;
}

bool Solver::isInZone(Coord zone, int number, int grid[][9])
{
    if(zone.x >= 0 && zone.y >= 0 && zone.x <= 2 && zone.y <=2)
    {
        for(int i=(3*zone.x); i<=(3*zone.x+2); i++)
        {
            for(int j=(3*zone.y); j<=(3*zone.y+2); j++)
            {
                if(grid[i][j] == number)
                {
                    return true;
                }
            }
        }
    }
    return false;
}

bool Solver::checkData(int grid[][9])
{
    int a=0;
    for(int i=0; i<=8; i++)
    {
        for(int j=0; j<=8; j++)
        {
            if(grid[i][j]!=0)
            {
                a++;
            }
        }
    }
    if(a>=10)
    {
        return true;
    }
    else
    {
        return false;
    }
}
