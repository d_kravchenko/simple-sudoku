#-------------------------------------------------
#
# Project created by QtCreator 2013-11-30T22:56:39
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = sudoku
TEMPLATE = app


SOURCES += main.cpp \
    sudoku.cpp \
    solver.cpp

HEADERS  += \
    sudoku.h \
    solver.h \
    exceptions.h

FORMS    += \
    sudoku.ui

RESOURCES += \
    sudoku.qrc
